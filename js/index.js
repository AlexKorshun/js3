"use strict";

let valid = false;
let input_a = 0;
let input_b = 0;
let operation = "+";
do {
    valid = true;
    input_a = prompt("Enter the first number please", input_a);
    if (!Number.isInteger(Number(input_a))) {
        alert("Your first number is not integer!");
        valid = false;
        continue;
    }
    input_b = prompt("Enter the second number please", input_b);
    if (!Number.isInteger(Number(input_b))) {
        alert("Your second number is not integer!");
        valid = false;
    }
    operation = prompt("Enter the operation (+, -, *, /)", operation);
    if (operation!=="+"&&operation!=="-"&&operation!=="*"&&operation!=="/") {
        alert("Your operation is wrong!");
        valid = false;
    }
} while (!valid)

alert("Answer is "+input_a+" "+operation+" "+ input_b + " = "+calculate(input_a,input_b,operation));

function calculate(a,b,operation) {
    let answer = 0;

    switch (operation) {
    case "+":
        answer=Number(a)+Number(b);
        break;
    case "-":
        answer=a-b;
        break;
    case "*":
        answer=a*b;
        break;
    case "/":
        answer=a/b;
        break;
    }
    return answer;
}






